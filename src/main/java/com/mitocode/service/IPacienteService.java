package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Paciente;

public interface IPacienteService {

	int registrar(Paciente paciente);

	int modificar(Paciente paciente);

	void eliminar(int idPaciente);

	Paciente listarId(int idPaciente);

	List<Paciente> listar();
}
