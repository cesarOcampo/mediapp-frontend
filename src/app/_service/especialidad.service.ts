

import { HOST } from './../_shared/var.constant';
import { Especialidad } from './../_model/especialidad';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EspecialidadService {
  url: string = `${HOST}/especialidad`;
  //pacientes: Paciente[] = [];

  constructor(private http: HttpClient) {    
    /*let p = new Paciente();
    p.idPaciente = 1;
    p.nombres = "Mito";
    p.apellidos = "Code";
    this.pacientes.push(p);

    p = new Paciente();
    p.idPaciente = 2;
    p.nombres = "Jaime";
    p.apellidos = "Medina";
    this.pacientes.push(p);*/
  }
  
  getlistarEspecialidad(){
    //return this.pacientes;
   //Observable
    return this.http.get<Especialidad[]>(`${this.url}/listar`);
  }
}
