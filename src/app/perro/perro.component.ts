import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-perro',
  templateUrl: './perro.component.html',
  styleUrls: ['./perro.component.css']
})
export class PerroComponent implements OnInit {

  @Input() nombre: string;
  @Input() raza: string;
  @Output() emisor = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  ladrar(){
    this.emisor.emit('¡Guau!');
  }

}
