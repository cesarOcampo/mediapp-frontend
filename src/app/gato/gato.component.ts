import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-gato',
  templateUrl: './gato.component.html',
  styleUrls: ['./gato.component.css']
})
export class GatoComponent implements OnInit {

  @Output() emisor = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  maullar(){
    this.emisor.emit('¡Miau!');
  }

}
