package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dao.IConsultaDAO;
import com.mitocode.dao.IConsultaExamenDAO;
import com.mitocode.model.Consulta;
import com.mitocode.service.IConsultaService;
import com.mitocode.util.ConsultaListaExamen;

//@Transactional
@Service
public class ConsultaServiceImpl implements IConsultaService{
	
	@Autowired
	private IConsultaDAO dao;
	
	@Autowired
	private IConsultaExamenDAO ceDAO;

	@Transactional
	@Override
	public Consulta registrar(ConsultaListaExamen dto) {
		Consulta cons = new Consulta();
		try {
			dto.getConsulta().getDetalleConsulta().forEach(d -> d.setConsulta(dto.getConsulta()));
			cons = dao.save(dto.getConsulta());
			/*for(Examen ex : dto.getLstExamen()) {
				ceDAO.registrar(idConsulta, idExamen)
			}*/			
			dto.getLstExamen().forEach(e -> ceDAO.registrar(dto.getConsulta().getIdConsulta(), e.getIdExamen()));
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		return cons;
	}

	@Override
	public void modificar(Consulta consulta) {
		dao.save(consulta);
		
	}

	@Override
	public void eliminar(int idConsulta) {
		dao.delete(idConsulta);		
	}

	@Override
	public Consulta listarId(int idConsulta) {
		return dao.findOne(idConsulta);
	}

	@Override
	public List<Consulta> listar() {
		return dao.findAll();
	}

}
