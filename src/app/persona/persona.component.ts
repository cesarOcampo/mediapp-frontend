import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent {

  @Input() mensaje: string;

  saludo: string;
  dni: string;
  lista: string[] = [];
  estadoBotonAgregar: boolean = true;

  teclear(event){    
    this.saludo = event.target.value;
    if(this.saludo.length > 0){
      this.estadoBotonAgregar = false;
    }else{
      this.estadoBotonAgregar = true;
    }
  }

  agregar(){
    this.lista.push(this.dni);
  }

  remover(index: number){
    this.lista.splice(index, 1);
  }

  recibirMaullido(event){
    console.log(event);
  }
}
